## YTicket con AngularJS y SB-Admin

El proyecto descargado fue modificado adecuando los archivos a la estructura de la pagina.
En esta documentacion se indicará cuales son los archivos que he creado.

## Lista de archivos
1. sidebar-title.js - sidebar-title.html : Ubicado en la parte superior de la lista de menu
   Copiado de: sidebar-search.js - sidebar-search.html
   
2. sidebar-left.js - sidebar-left.js: Lista de opciones ubicado en la parte de la izquierda de la pagina.
   Copiado de: sidebar.js - sidebar.html
   
3. table-ticket.html: Resultados obtenidos del servicio.
   Copiado de: table.html

4. form-ticket.html: Formulario de registro
   Copiado de: form.html