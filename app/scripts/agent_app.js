'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
  ])
  .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','$httpProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,$httpProvider) {
   $httpProvider.defaults.withCredentials = true;
   $httpProvider.defaults.transformRequest = function(data){
        if (data === undefined) {
            return data;
        }
        return $.param(data);
    }    
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });    
    
    $urlRouterProvider.otherwise('/ticket/inicio');

    $stateProvider
      .state('agente', {
        url:'/ticket',
        templateUrl: 'views/dashboard/main.html',
        controller: function($scope,util_service) {
             util_service.rol = "Agente";
             util_service.rol_id = 2;
        },
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-menu/header-menu.js',
                    'scripts/directives/sidebar/sidebar-left.js',
                    'scripts/directives/sidebar/sidebar-title/sidebar-title.js',
                    'scripts/services/utilService.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
      .state('agente.inicio',{
        templateUrl:'views/table-ticket.html',
        url:'/inicio',
        controller:'TicketTableCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/tableTicketController.js']
            })
          }
        }
    })
       .state('agente.detalle',{
        templateUrl:'views/detalle-ticket.html',
        url:'/detalle/:code',
        controller:'TicketDetalleCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/detalleTicketController.js']
            })
          }
        }
    })
       .state('agente.perfil',{
        templateUrl:'views/form-user-profile.html',
        url:'^/perfil',
        controller:'TicketProfileCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/formUserProfileController.js']
            })
          }
        }
    })
       .state('agente.reporte_actividad',{
        templateUrl:'views/report-activity-ticket.html',
        url:'^/reporte/reporte_actividad',
        controller:'ChartATCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/reportActivityTicketController.js']
            })
          }
        }
    })
       .state('agente.reporte_prediccion',{
        templateUrl:'views/report-prediccion-tema.html',
        url:'^/reporte/reporte_prediccion',
        controller:'ChartPCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name:'chart.js',
              files:[
                'bower_components/angular-chart.js/dist/angular-chart.min.js',
                'bower_components/angular-chart.js/dist/angular-chart.css'
              ]
            }),
            $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/reportPrediccionController.js']
            })
          }
        }
    })
}]);