'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.service('util_service', function($window,$location) {
      this.url_ft = "http://www.filltext.com/?";
      this.url = "http://" + $window.location.host;
      this.url_service = "http://jmwebserver.ddns.net";
      this.full_page = true;
      this.rol_type = 0;
      this.collapseTicket = 0;
      this.showDetalleTicket = false;
      this.userType = [{rol_id:-1,rol_name:'Prohibido'},{rol_id:1,rol_name:'Administrador'},{'rol_id':2,rol_name:'Agente'},{'rol_id':3,rol_name:'Usuario'}];
      this.getRolInfo = function() {
          var rolInfo = -1;
          if ($window.sessionStorage["usrtype"]&&$window.sessionStorage["usrtype"]!=null) {
              rolInfo = $window.sessionStorage["usrtype"];
              rolInfo = parseInt(rolInfo, 10);
              if(isNaN(rolInfo)){
                  rolInfo = -1;
              }
          }
          if(rolInfo==-1){
              console.info("Resettt...!!!");
//              $window.location.href= "/";
          }
          return this.userType[rolInfo];
      }
      this.getUserData = function() {
          var userInfo = null;
          if ($window.sessionStorage["userInfo"]&&$window.sessionStorage["userInfo"]!=null) {
                userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            }
          return userInfo;
      }
      this.getFullPageState = function() {
          return this.full_page;
      }
      this.saveUserData = function(user) {
          $window.sessionStorage["usrtype"] = JSON.stringify(user.user_type_id);
          $window.sessionStorage["userInfo"] = JSON.stringify(user);
      }
      this.goToHome = function(user_type) {
        if(user_type==1){
            $location.path('/administrador/agente/ver');
        }else if(user_type==2){
            $window.location.href= "/agente.html";
        }else if(user_type==3){
            $window.location.href= "/usuario.html";
        }else{
            $location.path('/login');
        }
      }
      this.formats = ['dd-MMMM-yyyy','dd-MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];        
      this.getNow = function(withMonthName) {
        var monthNames = [
        "Enero", "Febrero", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Setiembre", "Octubre",
        "Noviembre", "Diciembre"
        ];
        var date = new Date();
        var day = date.getDate();
        day = day>9?day:"0"+day;
        var monthIndex = date.getMonth()+1;
        var year = date.getFullYear();
        var month = withMonthName?monthNames[monthIndex-1]:monthIndex>9?monthIndex:"0"+monthIndex;
        return day+"-"+month+"-"+year;
      };
    })
    .factory("http_api", ["$http","$q","$window","$location","util_service",function ($http, $q, $window,$location,util_service) {
        var userInfo;

        var transform = function(data){
            return $.param(data);
        }
        function post(url,params) {
            var deferred = $q.defer();
            $http.post(util_service.url_service+url, params, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: transform
            }).success(function(resp) {
                console.info(resp);
                deferred.resolve(resp.data);
            }).error(function(data, status) {
                console.info(data);
                if(status==422){
                    deferred.reject(data.errors);
                }else if(status==401){// Sin Sesión
                    $window.location.href= "/";
                }else if(status==403){// Sin Permiso
                    $window.location.href= "/";
                }else{
                    deferred.reject(data);
                }
            })
            .finally(function() {            
            });
            return deferred.promise;
        }
        function update(url,params) {
            var deferred = $q.defer();
            $http.put(util_service.url_service+url, params, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                transformRequest: transform
            }).success(function(resp) {
                console.info(resp);
                deferred.resolve(resp.data);
            }).error(function(data, status) {
                console.info(data);
                if(status==422){
                    deferred.reject(data.errors);
                }else if(status==401){// Sin Sesión
                    $window.location.href= "/";
                }else if(status==403){// Sin Permiso
                    $window.location.href= "/";
                }else{
                    deferred.reject(data);
                }
            })
            .finally(function() {            
            });
            return deferred.promise;
        }
        function get(url) {
            var deferred = $q.defer();
            $http.get(util_service.url_service+url, {
                headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
            }).success(function(resp) {
                console.info(resp);
                deferred.resolve(resp);
            }).error(function(data, status) {
                console.info(data);
                deferred.reject(data.error);
            })
            .finally(function() {            
            });
            return deferred.promise;
        }

        function logout() {
            get("/api/auth/logout").then(function (data) {
                util_service.full_page = true;
                $window.sessionStorage["userInfo"]=null;
                $window.sessionStorage["usrtype"]=null;
                $window.location.href= "/";
            }, function (error) {
                util_service.full_page = true;
                $window.sessionStorage["userInfo"]=null;
                $window.sessionStorage["usrtype"]=null;
                $window.location.href= "/";
            });
        }
        function check() {
            get("/api/auth/check").then(function (data) {
                data = data.data;
                util_service.full_page=false;
                util_service.saveUserData(data);
                util_service.goToHome(data.user_type_id);
            }, function (error) {
                util_service.full_page=true;
                 $location.path('/login');
            });
        }
        return {
            post: post,
            get: get,
            update: update,
            logout: logout,
            check: check
        };
    }]);