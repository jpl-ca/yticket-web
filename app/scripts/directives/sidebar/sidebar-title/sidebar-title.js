'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
  .directive('sidebarTitle',function() {
    return {
      templateUrl:'scripts/directives/sidebar/sidebar-title/sidebar-title.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope,util_service){
        $scope.selectedMenu = 'home';
        $scope.rol = util_service.getRolInfo();
      }
    }
  });