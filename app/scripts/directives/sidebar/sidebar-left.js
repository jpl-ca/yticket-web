'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
  .directive('sidebar',['$location',function() {
    return {
      templateUrl:'scripts/directives/sidebar/sidebar-left.html',
      restrict: 'E',
      replace: true,
      scope: {
      },
      controller:function($scope,util_service){
        $scope.selectedMenu = 'dashboard';
        $scope.rol = util_service.getRolInfo();
        $scope.rol_id = $scope.rol.rol_id;
        $scope.showDetalleTicket = util_service.showDetalleTicket;
        $scope.collapseVar = util_service.collapseTicket;
        $scope.check = function(x){
          if(x==$scope.collapseVar)
            $scope.collapseVar = 0;
          else
            $scope.collapseVar = x;
        };
      }
    }
  }]);