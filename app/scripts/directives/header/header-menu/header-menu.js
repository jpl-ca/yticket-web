'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */
angular.module('sbAdminApp')
	.directive('headerMenu',function(){
		return {
        templateUrl:'scripts/directives/header/header-menu/header-menu.html',
        restrict: 'E',
        replace: true,
        controller:function($scope,http_api){
            $scope.logout = function(){
                http_api.logout();
            }
          }
    	}
    });