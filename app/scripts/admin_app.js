'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'angular-loading-bar',
  ])
  .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','$httpProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,$httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $ocLazyLoadProvider.config({
      debug:false,
      events:true
    });
    $urlRouterProvider.otherwise('/inicio');
      
    $stateProvider
      .state('yticket', {
        url:'/page',
        templateUrl: 'views/dashboard/main.html',
        controller: function($scope,util_service) {
            $scope.full_page = util_service.full_page;
            console.info($scope.full_page);
            $scope.$watch(function () { return util_service.getFullPageState();},
               function (value) {
                   $scope.full_page = value;
               }
            );
        },
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'scripts/directives/header/header.js',
                    'scripts/directives/header/header-menu/header-menu.js',
                    'scripts/directives/sidebar/sidebar-left.js',
                    'scripts/directives/sidebar/sidebar-title/sidebar-title.js',
                    'scripts/services/utilService.js'
                    ]
                }),
                $ocLazyLoad.load(
                {
                   name:'toggle-switch',
                   files:["bower_components/angular-toggle-switch/angular-toggle-switch.min.js",
                          "bower_components/angular-toggle-switch/angular-toggle-switch.css"
                      ]
                }),
                $ocLazyLoad.load(
                {
                  name:'ngAnimate',
                  files:['bower_components/angular-animate/angular-animate.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngCookies',
                  files:['bower_components/angular-cookies/angular-cookies.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngResource',
                  files:['bower_components/angular-resource/angular-resource.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngSanitize',
                  files:['bower_components/angular-sanitize/angular-sanitize.js']
                })
                $ocLazyLoad.load(
                {
                  name:'ngTouch',
                  files:['bower_components/angular-touch/angular-touch.js']
                })
            }
        }
    })
       .state('yticket.inicio',{
        url:'^/inicio',
        controller: function($scope,util_service,http_api) {
            http_api.check();
        }
    })
       .state('yticket.login',{
        templateUrl:'views/login.html',
        url:'^/login',
        controller:'LoginCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/loginController.js']
            })
          }
        }
     })
       .state('yticket.agente_ver',{
        templateUrl:'views/table/ver-agente.html',
        url:'^/administrador/agente/ver',
        controller:'AgenteCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/verAgenteController.js']
            })
          }
        }
    })
       .state('yticket.agente_nuevo',{
        templateUrl:'views/form/agente-registro.html',
        url:'^/administrador/agente/agregar',
        controller:'AgenteCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/registroAgenteController.js']
            })
          }
        }
    })
       .state('yticket.tema_ayuda_registro',{
        templateUrl:'views/form/tema-ayuda-registro.html',
        url:'^/administrador/tema_ayuda/agregar',
        controller:'TemaAyudaCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/registroTemaAyudaController.js']
            })
          }
        }
    })
       .state('yticket.tema_ayuda_ver',{
        templateUrl:'views/table/ver-tema-ayuda.html',
        url:'^/administrador/tema_ayuda/ver',
        controller:'TemaAyudaCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/verTemaAyudaController.js']
            })
          }
        }
    })
       .state('yticket.dpto_ver',{
        templateUrl:'views/table/ver-departamento.html',
        url:'^/administrador/departamento/ver',
        controller:'DepartamentoCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/verDepartamento.js']
            })
          }
        }
    })
       .state('yticket.dpto_nuevo',{
        templateUrl:'views/form/departamento-registro.html',
        url:'^/administrador/departamento/nuevo',
        controller:'DepartamentoCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/registroDepartamento.js']
            })
          }
        }
    })
       .state('yticket.config_accesos',{
        templateUrl:'views/form/accesos.html',
        url:'^/administrador/config/acceso',
        controller:'AccesoCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/accessoConfigController.js']
            })
          }
        }
    })
       .state('yticket.config_alert_notif',{
        templateUrl:'views/form/alerta-notificacion.html',
        url:'^/administrador/config/alerta-notificacion',
        controller:'AlertaNotifCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/alerta-notificacionConfigController.js']
            })
          }
        }
    })
       .state('yticket.config_empresa',{
        templateUrl:'views/form/empresa.html',
        url:'^/administrador/config/empresa',
        controller:'EmpresaCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/empresaConfigController.js']
            })
          }
        }
    })
       .state('yticket.config_sistema',{
        templateUrl:'views/form/sistema.html',
        url:'^/administrador/config/sistema',
        controller:'SistemaCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/sistemaConfigController.js']
            })
          }
        }
    })
       .state('yticket.config_tickets',{
        templateUrl:'views/form/tickets.html',
        url:'^/administrador/config/tickets',
        controller:'TicketsCtrl',
        resolve: {
          loadMyFile:function($ocLazyLoad) {
            return $ocLazyLoad.load({
                name:'sbAdminApp',
                files:['scripts/controllers/admin/ticketsConfigController.js']
            })
          }
        }
    })
}]);