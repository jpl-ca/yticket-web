'use strict';
/**
 * @ngdoc overview
 * @name sbAdminApp
 * @description
 * # sbAdminApp
 *
 * Main module of the application.
 */
angular
  .module('sbAdminApp', [
    'oc.lazyLoad',
    'ui.router',
    'ui.bootstrap',
    'ngCookies',
    'angular-loading-bar',
  ])
  .config(['$stateProvider','$urlRouterProvider','$ocLazyLoadProvider','$httpProvider',function ($stateProvider,$urlRouterProvider,$ocLazyLoadProvider,$httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $ocLazyLoadProvider.config({
      debug:false,
      events:true,
    });    
    
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('login',{
        templateUrl:'views/login.html',
        url:'/',
        controller:'LoginCtrl',
        resolve: {
            loadMyDirectives:function($ocLazyLoad){
                return $ocLazyLoad.load(
                {
                    name:'sbAdminApp',
                    files:[
                    'scripts/services/utilService.js'
                    ]
                }),
                $ocLazyLoad.load({
                    name:'sbAdminApp',
                    files:['scripts/controllers/loginController.js']
                })
            }
        }
    })
}]);