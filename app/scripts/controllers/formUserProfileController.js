'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('TicketProfileCtrl', function($scope,$http,$window,util_service,http_api) {
    $scope.utilService = util_service;
    util_service.showDetalleTicket = false;
    $scope.title_page = "Perfil de Usuario";
    $scope.user = util_service.getUserData();
    var params = {
        user_type_id : $scope.user.user_type_id,
        name : $scope.user.name,
        email : $scope.user.email,
        password : "",
        password_confirmation : ""
    };
    $scope.ActualizarDatos = function(){
        http_api.update("/api/user", params)
        .then(function (data) {
        }, function (error) {
        });
    }
});