'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('TicketTableCtrl', function($scope,$http,$window,util_service,http_api) {
    $scope.utilService = util_service;
    $scope.title_page = "Tickets";
    $scope.tickets = [];
    util_service.collapseTicket=1;
    util_service.showDetalleTicket = false;
    
    $scope.verDetalle = function(code) {
        if(util_service.rol_id==1)
            $window.location.href = util_service.url+"/usuario.html#/ticket/detalle/"+code;
        else if(util_service.rol_id==2)
            $window.location.href = util_service.url+"/agente.html#/ticket/detalle/"+code;
    };    
    
      $scope.totalItems = 0;
      $scope.currentPage = 0;
      $scope.num_by_page = 25;
      http_api.get("/api/ticket?_paginate="+$scope.num_by_page+"&_page=1").then(function (data) {
            $scope.tickets = data.data;
            $scope.totalItems = data.meta.last_page*10;
            $scope.currentPage = data.meta.current_page;
        }, function (error) {
      });

      $scope.setPage = function (pageNo) {
             $scope.currentPage = pageNo;
      };
      $scope.pageChanged = function() {
            http_api.get("/api/ticket?_paginate="+$scope.num_by_page+"&_page="+$scope.currentPage).then(function (data) {
            $scope.tickets = data.data;
            $scope.totalItems = data.meta.last_page*10;
            $scope.currentPage = data.meta.current_page;
        }, function (error) {
        });
      };
});