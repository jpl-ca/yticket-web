'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('LoginCtrl', function($scope,$http,$stateParams,$window,$state,util_service,http_api) {
    util_service.full_page=true;
    $scope.email = "yticket@yopmail.com";
//    $scope.email = "qhoeger@dietrich.com";
//    $scope.email = "natalie63@gmail.com";
    $scope.pass = "password";
    $scope.rememberme = false;
    $scope.msg_error = "";
    
    http_api.check();
    
    $scope.sendLogin = function() {
        $scope.msg_error = "";
        if($scope.email==null||$scope.pass==null){
            alert("Datos incompletos!");
            return;
        }
        var params = {
//            remember : $scope.rememberme?1:0,
            email : $scope.email,
            password : $scope.pass
		};
        http_api.post("/api/auth/login", params)
        .then(function (data) {
//            console.info("Send:");
//            console.info(data);
//            console.info(data.data);
            util_service.saveUserData(data);
            util_service.full_page=false;
        }, function (error) {
            $scope.msg_error = error.detail[0];
        });
    };
});