'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
    .controller('ChartPCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.title = "Predicción de Temas de Ayuda";
        $scope.bar = {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            series: ['2014', '2015'],
            data: [
               [65, 59, 80, 81, 56, 55, 40, 48, 40, 19, 86, 27],
               [28, 48, 40, 19, 86, 27, 90, 80, 81, 56, 55, 40]
            ]

        };
        
        $scope.periodos = [
           {id: '000001', name: 'Averia en red'},
           {id: '000003', name: 'Averia en otra red'},
           {id: '000004', name: 'Averia extra'}
        ];
}]);