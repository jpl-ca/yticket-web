'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('TicketFormCtrl', function($scope,$http,$stateParams,$filter,util_service,http_api) {
    $scope.utilService = util_service;
    util_service.collapseTicket=1;
    $scope.format = util_service.formats[1];
    util_service.showDetalleTicket = false;
    
    /** Inicializar $scope*/
    $scope.vista_previa=false;
    $scope.serv_aff=1;
    $scope.asunto="";
    $scope.detalle="";
    $scope.user = util_service.getUserData();
    http_api.get("/api/topic").then(function (data) {
        $scope.tipo_evento = data.data;
    }, function (error) {
    });
    http_api.get("/api/engineer").then(function (data) {
        $scope.ingeniero = data.data;
    }, function (error) {
    });
    http_api.get("/api/network-type").then(function (data) {
        $scope.tipo_red = data.data;
    }, function (error) {
    });
    http_api.get("/api/shift").then(function (data) {
        $scope.turno = data.data;
    }, function (error) {
    });
    $scope.tipoEventoOption = null;
    $scope.ingenieroCargoOption = null;
    $scope.turnoOption = null;
    $scope.tipoRedOption = null;
    
    
    /**  Funcion de botones */
    $scope.vistaPrevia = function() {
        $scope.vista_previa=true;
    };
    
    $scope.alert = null;
    $scope.closeAlert = function(index) {
        $scope.alert = null;
    };
    $scope.crearTicket = function(asd) {
        if($scope.tipoEventoOption==null||$scope.turnoOption==null||$scope.ingenieroCargoOption==null||$scope.tipoRedOption==null){
            alert("Datos incompletos!");
            return;
        }
        var start_date = null;
        var end_date = null;
        if($scope.serv_aff==1){
//            start_date = $filter('date')($scope.dtInicio, 'yyyy-MM-dd 00:00:00');
//            end_date = $filter('date')($scope.dtFin, 'yyyy-MM-dd 00:00:00');
            start_date = $("#dtInicio").val() + " 00:00:00";
            end_date = $("#dtFin").val() + " 00:00:00";
        }        
        var params = {
				user_id : $scope.user.id,
				shift_id : $scope.turnoOption.id,
				engineer_id : $scope.ingenieroCargoOption.id,
				network_type_id : $scope.tipoRedOption.id,
				topic_id : $scope.tipoEventoOption.id,
                
				affected_services : $scope.serv_aff,
				start_date : start_date,
				end_date : end_date,
				call_time : $filter('date')($scope.hora_llamada, 'yyyy-MM-dd HH:mm:ss'),
				subject : $scope.asunto,
				detail : $scope.detalle
		};
        $scope.alert = null;
        http_api.post("/api/ticket", params)
        .then(function (data) {
        }, function (error) {
           $scope.alert = { type: 'danger', msg: error.detail[0] };
        });
        
    };
    $scope.cancelar = function() {
        if($scope.vista_previa)$scope.vista_previa=false;
    };
    
    
    /** Inicializar DatePicker & TimePicker*/
    var coeff = 1000 * 60 * 5;
    var date = new Date();
    var roundTime = new Date(Math.round(date.getTime() / coeff) * coeff)    
    $scope.hora_llamada = roundTime;
    $scope.hstep = 1;
    $scope.mstep = 5;
    $scope.ismeridian = true;
    $scope.today = function() {
      $scope.dtInicio = $scope.dtInicio ? null : new Date();
      $scope.dtFin = $scope.dtFin ? null : new Date();
    };
    
    $scope.today();
    
    $scope.minDateI="2010-01-01";
    $scope.minDateF="2010-01-01";
    
    $scope.maxDateI="20220-12-31";
    $scope.maxDateF="2020-12-31";
    
    $scope.clear = function () {
//      $scope.dtInicio = null;
//      $scope.dtFin = null;
//        $scope.dtInicio = new Date();
//        $scope.dtFin = new Date();
    };
    $scope.openedStart = false;
    $scope.openedEnd = false;
    
    $scope.openI = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedStart = true;
      setTimeout(function() { $scope.openedStart = false; }, 10);
    };
    $scope.openF = function($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.openedEnd = true;
      setTimeout(function() { $scope.openedEnd = false; }, 10);
    };
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      'show-button-bar': 'false',
      onClose: function(){
        console.info("closinggg");
      }
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[1];
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 2);
    $scope.events =
        [
          {
            date: tomorrow,
            status: 'full'
          },
          {
            date: afterTomorrow,
            status: 'partially'
          }
      ];
});