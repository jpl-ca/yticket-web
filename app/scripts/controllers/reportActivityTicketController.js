'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
    .controller('ChartATCtrl', ['$scope', '$timeout', function ($scope, $timeout) {
        $scope.title = "Actividad de Tickets";
        $scope.line = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            series: ['Abierto', 'Resuelto', 'Cerrado'],
            data: [
                   [65, 59, 80, 81, 56, 55, 40],
                   [28, 48, 40, 19, 86, 27, 90],
                   [58, 33, 48, 40, 19, 86, 27]
                 ],
            onClick: function (points, evt) {
                console.log(points, evt);
            }
        };
        $scope.periodos = [
           {id: '000001', name: 'Averia en red'},
           {id: '000003', name: 'Averia en otra red'},
           {id: '000004', name: 'Averia extra'}
        ];
}]);