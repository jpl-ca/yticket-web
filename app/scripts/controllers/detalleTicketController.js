'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('TicketDetalleCtrl', function($scope,$http,$stateParams,util_service,http_api) {
    $scope.utilService = util_service;
    $scope.title_page = "Ticket";
    $scope.code_ticket = $stateParams.code;
    $scope.user = util_service.getUserData();
    $scope.asunto = "Lorem,Ipsum,Lorem,Ipsum,Lorem,IpsumLorem,Ipsum,Lorem,Ipsum";
    $scope.new_comment = "";
    $scope.list_dialog_ticket = [];
    $scope.dialog_ticket = {};
    $scope.rol_id = util_service.rol_id;
    util_service.collapseTicket=1;
    util_service.showDetalleTicket = false;
    http_api.get("/api/ticket?q=where=code,"+$scope.code_ticket).then(function (data) {
        $scope.list_dialog_ticket = data.data;
    }, function (error) {
    });
    $scope.format = "'dd-MMMM-yyyy'";
    $scope.addComment = function() {
        $scope.dialog_ticket.comment_id = 10;
        $scope.dialog_ticket.subject = $scope.new_comment;
        $scope.dialog_ticket.created_at = util_service.getNow();
        $scope.dialog_ticket.user = {name:$scope.user.name};
        $scope.list_dialog_ticket.push($scope.dialog_ticket);
        $scope.dialog_ticket = {};
        $scope.new_comment = "";
    };
    $scope.clearComment = function() {
        $scope.new_comment = "";
    };
});