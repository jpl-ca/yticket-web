'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:AgenteCtrl
 * @description
 * # AgenteCtrl
 * Controller to verAgente.html
 */
angular.module('sbAdminApp')
  .controller('AlertaNotifCtrl', function($scope,$http,$window,util_service,http_api) {
    util_service.full_page = false;
    $scope.utilService = util_service;
    util_service.collapseTicket=2;
});