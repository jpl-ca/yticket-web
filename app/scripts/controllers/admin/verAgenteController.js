'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:AgenteCtrl
 * @description
 * # AgenteCtrl
 * Controller to verAgente.html
 */
angular.module('sbAdminApp')
  .controller('AgenteCtrl', function($scope,$http,$window,util_service,http_api) {
    util_service.full_page = false;
    $scope.utilService = util_service;
    $scope.title_page = "Agentes";
    util_service.collapseTicket=1;
    $scope.users = [];
    http_api.get("/api/user?user_type_id=2").then(function (data) {
        $scope.users = data.data;
    }, function (error) {
    });
});