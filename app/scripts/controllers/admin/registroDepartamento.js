'use strict';
angular.module('sbAdminApp')
  .controller('DepartamentoCtrl', function($scope,$http,$window,util_service,http_api) {
    util_service.full_page = false;
    $scope.utilService = util_service;
    util_service.collapseTicket=1;
    
    $scope.alert = null;
    $scope.name_dep = "";
    $scope.RegistrarDepartamento = function(){
        var params = {
            name : $scope.name_dep
        }
        $scope.alert = null;
        http_api.post("/api/user", params)
        .then(function (data) {
        }, function (error) {
            $scope.alert = { type: 'danger', msg: error.detail[0] };
        });
    }
});