'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:AgenteCtrl
 * @description
 * # AgenteCtrl
 * Controller to verAgente.html
 */
angular.module('sbAdminApp')
  .controller('AgenteCtrl', function($scope,$http,$window,util_service,http_api) {
    util_service.full_page = false;
    $scope.utilService = util_service;
    $scope.title_page = "Agentes";
    util_service.collapseTicket=1;
    http_api.get("/api/department").then(function (data) {
        $scope.department = data.data;
    }, function (error) {
    });
    $scope.department_agent = null;
    $scope.pass = null;
    $scope.pass_confirm = null;
    $scope.agente = {};
    $scope.alert = null;
    $scope.closeAlert = function(index) {
        $scope.alert = null;
    };
    $scope.RegistrarAgente = function(){
        var params = {
                user_type_id : 2,
                department_id : $scope.department_agent.id,
                name : $scope.agente.name,
                email : $scope.agente.email
            }
        if($scope.pass!=""){
            params.password = $scope.pass;
            params.password_confirmation = $scope.pass_confirm;
        }
        $scope.alert = null;
//        console.info(params);
        http_api.post("/api/user", params)
        .then(function (data) {
        }, function (error) {
            $scope.alert = { type: 'danger', msg: error.detail[0] };
        });
    }
});