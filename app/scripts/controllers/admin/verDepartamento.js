'use strict';
angular.module('sbAdminApp')
  .controller('DepartamentoCtrl', function($scope,$http,$window,util_service,http_api) {
    util_service.full_page = false;
    $scope.utilService = util_service;
    $scope.title_page = "Departamentos";
    $scope.department = [];
    util_service.collapseTicket=1;
    console.info("Loading Deptmnt...!!!");
    http_api.get("/api/department").then(function (data) {
        $scope.department = data.data;
    }, function (error) {
    });
});